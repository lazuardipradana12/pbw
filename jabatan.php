<?php include 'header.php'; ?>

        <h1 class="mt-3 mb-3">Jabatan</h1>
        <?php if (! empty($_SESSION['username'])) { ?>
        <a href="formJabatan.php" class="btn btn-sm btn-info mb-3">Tambah</a>
        <?php } ?>
        <table class="table">
            <thead class="table-info">
                <tr>
                    <th>No</th>
                    <th>Jabatan</th>
                    <?php if (! empty($_SESSION['username'])) { ?>    
                    <th>Aksi</th>
                    <?php } ?>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    $sql = 'SELECT * FROM jabatan';
                    $query = mysqli_query($conn, $sql);
                    while ($row = mysqli_fetch_object($query)) {
                ?>
                
                <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->jabatan; ?></td> 
                    <?php if (! empty($_SESSION['username'])) { ?>    
                    <td>
                        <a href="formJabatan.php?id_jabatan=<?php echo $row->id_jabatan; ?>" class="btn btn-sm btn-warning");">Ubah</a>
                        <a href="deletejabatan.php?id_jabatan=<?php echo $row->id_jabatan; ?>" class="btn btn-sm btn-danger"onclick="return confirm('yakin nih di hapus?');">Hapus</a>
                    </td>
                    <?php } ?>
                </tr>

                <?php
                    } if (!mysqli_num_rows($query)) {
                        echo '<tr><td colspan="6" class="text-center">Tidak ada data.</td></tr>';
                    }
                ?>

            </tbody>
        </table>

<?php include 'footer.php'; ?>